import java.util.*;

public class Puzzle {

   private static Map<String, Word> theWords = new HashMap<>();
   private static int solutions = 0;
   private static String firstSolution = "";

   /**
    * Main method
    * @param args String array
    */
   public static void main (String[] args) {
      if (args.length < 3) {
         throw new RuntimeException("At least 3 words required! Got: " + args);
      }
      if (args.length > 3) {
         throw new RuntimeException("No more than 3 words allowed! Got: " + args);
      }
      if (args[2].length() < args[0].length() || args[2].length() < args[1].length()) {
         throw new RuntimeException("Puzzle sum word can not be of less length than the adders!" +
                 " Got: " + args[2] + ", which is too long in " + args);
      }
      if (args[2].length() > args[0].length() + 1 || args[2].length() > args[1].length() + 1) {
         throw new RuntimeException("Puzzle sum word length can not be longer than adder + 1!" +
                 " Got: " + args[2] + ", which is too long in " + args);
      }
      for (String arg : args) {
         if (arg.length() == 0) {
            throw new RuntimeException("Puzzle can not have empty words! Got: " + arg + " in " + args);
         }
         if (arg.length() > 18) {
            throw new RuntimeException("A word can not be longer than 18 characters! Got: " + arg + " in " + args);
         }
      }

      theWords.put("word1", new Word(args[0]));
      theWords.put("word2", new Word(args[1]));
      theWords.put("sum", new Word(args[2]));

      solvePuzzle(findUniqueLetters(args), new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});

      System.out.println("Puzzle " + Arrays.toString(args) + " result\n");
      if (solutions == 0) {
         throw new RuntimeException("The puzzle " + Arrays.toString(args) + " is not solvable!");
      } else if (solutions == 1) {
         System.out.println("This puzzle has a unique solution:\n" +
                 args[0] + " + " + args[1] + " = " + args[2] + "\n" + firstSolution + "\n");
      } else {
         System.out.println("Found " + solutions + " solution(s).\n" + "Example: \n" +
                 args[0] + " + " + args[1] + " = " + args[2] + "\n" + firstSolution + "\n");
      }
      theWords = new HashMap<>();
      solutions = 0;
      firstSolution = "";
   }

   /** Solve puzzle with backtracking
    * @param lettersToAssign unique letters in the puzzle
    * @param remainingNums possible values to assign to letters that are left
    */
   private static void solvePuzzle(String lettersToAssign, int[] remainingNums) {
      if (lettersToAssign.isEmpty()) {
         isAnswer(getValues());
         return;
      }
      for (int i : remainingNums) {
         if (checkIfStarter(lettersToAssign.charAt(0)) && i == 0) {
            continue;
         }
         updateValue(lettersToAssign.charAt(0), i);
         int[] otherNums = new int[remainingNums.length - 1];
         int counter = 0;
         for (int num : remainingNums) {
            if (num == i) {
               continue;
            }
            otherNums[counter] = num;
            counter++;
         }
         solvePuzzle(lettersToAssign.substring(1), otherNums);
         resetValue(lettersToAssign.charAt(0));
      }
   }

   /** Get all the unique letters from the puzzle
    * @param args given word puzzle
    * @return string of unique letters
    */
   private static String findUniqueLetters(String[] args) {
      StringBuilder str = new StringBuilder(args[0] + args[1] + args[2]);
      Set<Character> ch = new HashSet<>();
      int index = 0;
      for (int i = 0; i < str.length(); i++) {
         ch.add(str.charAt(i));
      }
      str = new StringBuilder();
      for (Character c: ch) {
         str.append(c.toString());
      }
      return str.toString();
   }

   /** Get values of all words of puzzle
    * @return array of values
    */
   private static long[] getValues() {
      long[] values = new long[theWords.values().size()];

      values[0] = theWords.get("word1").value();
      values[1] = theWords.get("word2").value();
      values[2] = theWords.get("sum").value();

      return values;
   }


   /** Update value of a letter
    * @param c character
    * @param value new value
    */
   private static void updateValue(char c, int value) {
      for (Word word : theWords.values()) {
         if (word.word.contains(String.valueOf(c))) {
            for (Letter letter : word.letters) {
               if (letter.letter == c) {
                  letter.value = value;
               }
            }
         }
      }
   }

   /** Check if the values match
    * @param values array of longs
    */
   private static void isAnswer(long[] values) {
      if (values[0] + values[1] == values[2]) {
         solutions++;
         if (firstSolution.isEmpty()) {
            firstSolution = values[0] + " + " + values[1] + " = " + values[2];
         }
      }
   }

   /** put letter value to 0
    * @param c character
    */
   private static void resetValue(char c) {
      for (Word word : theWords.values()) {
         if (word.word.contains(String.valueOf(c))) {
            for (Letter letter : word.letters) {
               if (letter.letter == c) {
                  letter.value = 0;
               }
            }
         }
      }
   }

   /** Check if the letter is at the start of a word
    * @param c character
    * @return boolean
    */
   private static boolean checkIfStarter(char c) {
      for (Word word : theWords.values()) {
         if (word.letters[0].letter == c) {
            return true;
         }
      }
      return false;
   }

   static class Letter {
      public char letter;
      public int value;

      Letter (char c) {
         this.letter = c;
         this.value = 0;
      }

      @Override
      public String toString() {
         return letter + ": " + value;
      }
   }

   static class Word {
      public String word;
      public int length;
      public Letter[] letters;

      Word (String word) {
         this.word = word;
         this.length = word.length();
         this.letters = new Letter[word.length()];

         int i = 0;
         for (char c : word.toCharArray()) {
            if (!Character.isAlphabetic(c)) {
               throw new RuntimeException("Letter must be in the alphabet! Got: " + c);
            }
            this.letters[i] = new Letter(c);
            i++;
         }
      }

      public long value() {
         StringBuilder value = new StringBuilder();
         for (Letter letter : letters) {
            value.append(letter.value);
         }
         return Long.parseLong(value.toString());
      }

      @Override
      public String toString() {
         return this.word;
      }
   }

   // Material used: https://www.geeksforgeeks.org/solving-cryptarithmetic-puzzles-backtracking-8/,
   // https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a
}
